"use strict";
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gREADY_STATE_REQUEST_DONE = 4;
  var gSTATUS_REQUEST_DONE = 200;
  const gURL="http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  $("#btn-check-voucher").on('click', onBtnCheckVoucherClick);
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // Hàm này để xử lý sự kiện nút check voucher click
  function onBtnCheckVoucherClick() {

    // B1: lấy giá trị nhập trên form, mã voucher (Thu thập dữ liệu)
    var vInputVoucher = $("#voucher");//truy vấn ô nhập mã giảm gia
    var vDivResultCheck = $("#div-result-check");//truy vấn thẻ div có id là div-result-check
    console.log("Subtask 1:");
    console.log("tôi lấy dữ liệu từ:");
    console.log("Input, id= " + vInputVoucher.attr('i') + " -placeholder= " + vInputVoucher.attr('placeholder'));
    console.log("Tác động vào: ");
    console.log("Div, id= " + vDivResultCheck.attr('id') + " - innerHTML= " + vDivResultCheck.html());

    var vVoucherObj = {
      maGiamGia: "",
    }
    //B1: lây dữ liệu trên ô input về
    var vVoucherCode = readData(vVoucherObj);
    //B2: validate
    var isValidateData = validateData(vVoucherObj);
    if (isValidateData) {
      var vXmlHttp = new XMLHttpRequest();
      //B3: gọi server
      sendVoucherToServer(vVoucherObj,vXmlHttp);
      vXmlHttp.onreadystatechange = function () {
        if (vXmlHttp.readyState === gREADY_STATE_REQUEST_DONE
          && vXmlHttp.status === gSTATUS_REQUEST_DONE) {
          processResponse(vXmlHttp);
        }
        else if (vXmlHttp.readyState === gREADY_STATE_REQUEST_DONE) {
          var vDivResultCheck = $("#div-result-check");
          vDivResultCheck.html("Ma giảm giá: " + vVoucherObj.maGiamGia + " không tồn tại");
        }
      }
      //B4: Hiển thị
    }
    }
    // var vVoucherCodeElement = document.getElementById("voucher");
    // var vVoucherCode = vVoucherCodeElement.value.trim();
    // B2: Validate data
    //   var vIsValidateData = validateData(vVoucherCode);
    //   if(vIsValidateData) {
    //     // B3: Tạo request và gửi mã voucher về server
    //     var bXmlHttp = new XMLHttpRequest();
    //     sendVoucherToServer(vVoucherCode, bXmlHttp);
    //     // B4: xu ly response khi server trả về
    //     bXmlHttp.onreadystatechange = function() {
    //       if(bXmlHttp.readyState === gREADY_STATE_REQUEST_DONE
    //         && bXmlHttp.status === gSTATUS_REQUEST_DONE) {
    //           processResponse(bXmlHttp);
    //         }
    //     }
    //   }
    // }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function readData(paramVoucherObj) {
      var vInputVoucher = $("#voucher");
      var vValueVoucher = vInputVoucher.val();
      paramVoucherObj.maGiamGia = vValueVoucher;
    }
    // Hàm này dùng để validate data
    function validateData(paramVoucherObj) {
      var vDivResultCheck = $("#div-result-check");//truy vấn thẻ div có id là div-result-check
      if (paramVoucherObj.maGiamGia === "") {
        //hiển thị câu thông báo lỗi
        vDivResultCheck.html("Mã Giảm Giá chưa được nhập");
        vDivResultCheck.prop('class', 'text-danger');
        return false;
      }
      else {
        //thay đổi class Css để đổi sang chữ màu đen binh thuong
        vDivResultCheck.prop('class', 'text-danger');
        vDivResultCheck.html("");
        return true;
      }
    }
    function sendVoucherToServer(paramVoucherObj, paramXmlVoucherRequest){
      paramXmlVoucherRequest.open("GET", gURL+paramVoucherObj.maGiamGia, true);
      paramXmlVoucherRequest.send();
    }
    function processResponse(paramXmlHttp){
      "use strict";
      var vJsonVoucherResponse=paramXmlHttp.responseText;
      console.log(vJsonVoucherResponse);
      var vVoucherResponseObj=JSON.parse(vJsonVoucherResponse);
      console.log(vVoucherResponseObj);
      var vDiscount=vVoucherResponseObj.phanTramGiamGia;
      var vDivResultCheck = $("#div-result-check");
      vDivResultCheck.html("Mã Giảm giá: "+vDiscount+"%" );
    }
    // input: ma voucher nguoi dung nhap vao
    // output: tra ve true neu ma voucher duoc nhap, nguoc lai thi return false
    // function validateData(paramVoucher) {
    //   var vResultCheckElement = document.getElementById("div-result-check");
    //   if(paramVoucher === "") {
    //     // Hiển thị câu thông báo lỗi ra
    //     vResultCheckElement.innerHTML = "Mã giảm giá chưa nhập!"
    //     // Thay đổi class css, để đổi màu chữ thành màu đỏ
    //     vResultCheckElement.className = "text-danger";
    //     return false;
    //   }

    //   // Thay đổi class css, để đổi màu chữ thành màu đen bình thường
    //   vResultCheckElement.className = "text-dark";
    //   return true;
    // }

    // // Ham thuc hien viec call api va gui ma voucher ve server
    // function sendVoucherToServer(paramVoucher, paramXmlVoucherRequest) {
    //   paramXmlVoucherRequest.open("GET", "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucher, true);
    //   paramXmlVoucherRequest.send();
    // }

    // // Hàm này được dùng để xử lý khi server trả về response
    // function processResponse(paramXmlHttp) {
    //   // B1: nhận lại response dạng JSON ở xmlHttp.responseText
    //   var vJsonVoucherResponse = paramXmlHttp.responseText;
    //   // B2: Parsing chuỗi JSON thành Object
    //   var vVoucherResObj = JSON.parse(vJsonVoucherResponse); 
    //   console.log(vJsonVoucherResponse);
    //   // B3: xử lý mã giảm giá dựa vào đối tượng vừa có
    //   var vDiscount = vVoucherResObj.phanTramGiamGia;
    //   var vResultCheckElement = document.getElementById("div-result-check");
    //   //discount - -1 nghĩa là mã giảm giá không tồn tại
    //   if(vDiscount === "-1") {  
    //     vResultCheckElement.innerHTML = "Không tồn tại mã giảm giá";
    //   } 
    //   else {
    //     vResultCheckElement.innerHTML  = "Mã giảm giá " + vDiscount +"%"; 
    //   } 
  



});